#!/bin/bash


WD=$PWD
rm *.patch
cd ~/dev-netsurf/workspace
DIRS=$(ls -1 | grep -v env.sh | grep -v inst)

for DIR in $DIRS
do
	echo "===> Generationg patch from $DIR"
	cd $DIR
	PATCH=$(git diff)
	if [ "$PATCH" != "" ]
	then
		echo "$PATCH" > ${WD}/${DIR}.patch
	fi
	cd ..
done
