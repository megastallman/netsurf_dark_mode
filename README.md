# Netsurf_dark_mode

A patch for Netsurf-GTK browser to view web-pages in dark mode.

## Getting started

1. [Setup Netsurf build environment, build and test the browser](https://www.netsurf-browser.org/developers/)
2. `cd ~/dev-netsurf/workspace/netsurf`
3. `patch -p1 < path_to_this_repo/netsurf_dark_mode/DarkMode.patch`
4. `make && nsgtk3`

Netsurf browser project page: https://www.netsurf-browser.org

## Openindiana/Solaris build

1. Install all required dependencies manually.
2. `cd /usr/include; ln -s cdio/bytesex.h`
3. Find Netsurf patches in `Solaris_patches` directory, patch the source.
4. Build according to official manuals.
